package com.example.catpet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.media.AudioManager;
import android.media.SoundPool;
import android.widget.Toast;

//import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.auth.FirebaseAuth;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import static java.lang.System.currentTimeMillis;

public class MainActivity extends AppCompatActivity {

    int timeKeeper;

    // Buttons
    private Button save;

    //Image Button
    private ImageButton food;
    private ImageButton sushi;
    private ImageButton fish;
    private ImageButton toy;
    private ImageButton chat;
    private ImageButton oyen;
    private ImageButton exit;
    private ImageButton dead;

    //Image View
    private ImageView munny;
    private ImageView longEat;
    private ImageView mediumEat;
    private ImageView smallEat;
    private ImageView longHappy;
    private ImageView mediumHappy;
    private ImageView smallHappy;
    private ImageView happy;
    private ImageView eat;
    private ImageView rectangle;
    private ImageView level;
    private ImageView age;

    private View view;
    //Toasts
    private ImageView heartheart;
    private ImageView notepad;


    //Contexts
    private Context context;

    //Boolean
    private boolean exist;

    //TextView
    private TextView levelText;
    private TextView munnyText;
    private TextView ageText;
    private TextView pricesushi;
    private TextView pricefish;
    //media player for background music
    MediaPlayer music;
    //soundeffect
    private SoundPlayer sound;
    //for logout
    private CharSequence name;

    Pet myCat;
    protected GameView gameView;
    @Override
    protected void onStart(){
        super.onStart();
        deadpet();

    }


    //All the visuals are here
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        String catname = intent.getStringExtra(addYourCat.EXTRA_TEXT);
        ;
        //    myCat = new Pet(catname,2,3,10,0,(int)(currentTimeMillis() / 1000),(int)(currentTimeMillis() / 1000), 100, 0, 100);

        gameView=new GameView(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //Landscape fixed
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Activity - main
        myCat = new Pet();
        music=MediaPlayer.create(this,R.raw.background);
        music.setVolume(0.3F,0.3F);
        music.setLooping(true);
        music.start();

        sound=new SoundPlayer(this);

        //Gets rid of top border (complete fullscreen)
        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        //ImageButton
        food = findViewById(R.id.food);
        sushi = findViewById(R.id.sushi);
        fish = findViewById(R.id.fish);
        toy = findViewById(R.id.toy);
        chat = findViewById(R.id.chat);
        oyen = findViewById(R.id.oyen);
        oyen.setImageResource(R.drawable.catanimation);
        dead = findViewById(R.id.dead);
        exit=findViewById(R.id.exit);
        AnimationDrawable catanimation = (AnimationDrawable)oyen.getDrawable();
        catanimation.start();

        //Boolean
        exist=true;

        //ImageView
        munny = findViewById(R.id.munny);
        longEat= findViewById(R.id.longeat);
        mediumEat = findViewById(R.id.mediumeat);
        smallEat = findViewById(R.id.smalleat);
        longHappy = findViewById(R.id.longhappy);
        mediumHappy = findViewById(R.id.mediumhappy);
        smallHappy= findViewById(R.id.smallhappy);
        happy = findViewById(R.id.happy);
        eat = findViewById(R.id.eat);
        rectangle = findViewById(R.id.rectangle);
        level = findViewById(R.id.level);
        age = findViewById(R.id.age);
        heartheart = findViewById(R.id.heartheart);
        notepad = findViewById(R.id.notepad);

        //Contexts

        context = getApplicationContext();

        //TextView
        levelText = findViewById(R.id.levelText);
        munnyText = findViewById(R.id.munnyText);
        ageText = findViewById(R.id.ageText);
        pricesushi = findViewById(R.id.pricesushi);
        pricefish = findViewById(R.id.pricefish);


        //Everything is visible before we hide them
        initialVisibility();
        deadpet();

        //Food bowl image button
        food.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sound.playclicksound();
                sushi.setVisibility(View.VISIBLE);
                fish.setVisibility(View.VISIBLE);
                rectangle.setVisibility(View.VISIBLE);
                food.setVisibility(View.GONE);
                pricesushi.setVisibility(View.VISIBLE);
                pricefish.setVisibility(View.VISIBLE);

                rectangle.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     initialVisibility();
                                                 }
                                             });

                //Sushi image button
                sushi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (myCat.getMunny() >= 50) {
                            //initialVisibility();
                            sound.playclicksound();
                            sound.playeatsound();
                            myCat.updateHunger(40);
                            hunger();
                            myCat.updateMunny(-50);
                            updateText();
                            initialVisibility();

                            myCat.updateAge((int) (currentTimeMillis() / 1000));
                        } else {
                            final View view1 = notepad;
                            view1.setVisibility(View.VISIBLE);
                            view1.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    view1.setVisibility(View.GONE);
                                }
                            }, 1000);
                        }
                        deadpet();
                        save();
                    }
                });



                //Fish image button
                fish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (myCat.getMunny() >= 25) {
                            // initialVisibility();
                            sound.playeatsound();
                            sound.playclicksound();
                            myCat.updateHunger(25);
                            hunger();
                            myCat.updateMunny(-25);
                            updateText();
                            initialVisibility();

                            myCat.updateAge((int) (currentTimeMillis() / 1000));
                        } else {
                            final View view2 = notepad;
                            view2.setVisibility(View.VISIBLE);
                            view2.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    view2.setVisibility(View.GONE);
                                }
                            }, 1000);
                        }

                        deadpet();
                        save();

                    }
                });


            }

        });



        oyen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(exist){
                    sound.playtouchcat();
                    final View view = heartheart;
                    view.setVisibility(View.VISIBLE);
                    view.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            view.setVisibility(View.GONE);
                            myCat.updateHappy(5);
                        }
                    }, 1000);
                }
                deadpet();
                save();
            }

        });

        //Toy image button
        toy.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, mazeclass.class);
                myCat.updateMunny(50);
                myCat.updateHappy(40);
                happy();
                updateText();
                startActivity(intent);
                deadpet();
                save();
            }
        });

        //Chat image button
        chat.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v){
                sound.playclicksound();
                Intent intent = new Intent(MainActivity.this, Chat.class);
                //Go to chat class
                startActivity(intent);
                deadpet();

            }
        });

       exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, login.class);
                startActivity(intent);
                       save();
            }
        });


    }

    @Override
    protected void onPause(){
        super.onPause();
        music.release();
        save();
        updateText();
        deadpet();


    }
    @Override
    protected void onResume() {
        super.onResume();
        timeKeeper=(int)(currentTimeMillis()/1000);
        myCat.updateAwayTime(timeKeeper);
        myCat.updateAge(timeKeeper);
        updateText();
        save();
        deadpet();
    }

    public void save(){
        //save variable in local file by converting variable to string
        //name,age,level,munny,agetime,awaytime,hunger,happiness
        String petInfo = myCat.getName() + " " + String.valueOf(myCat.getAge()) + " " + String.valueOf(myCat.getLevel()) + " " +
                String.valueOf(myCat.getMunny()) + " " +String.valueOf(myCat.getHunger())+" "+String.valueOf(myCat.getHappiness())+" "+String.valueOf(myCat.getAgeTime())+" "+String.valueOf(myCat.getAwayTime());

        FileOutputStream outputStream;
        try{
            outputStream = openFileOutput(myCat.getName() + ".pet", Context.MODE_PRIVATE);
            outputStream.write(petInfo.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startGame(View view){
        Intent intent = new Intent(this,GameActivity.class);
        startActivity(intent);
    }

    //Update Game
    private void updateGame(){
        myCat.updateAge((int)(currentTimeMillis()/1000));
        save();
        updateText();
        deadpet();

    }

    protected void initialVisibility(){
        //By default - VISIBLE
        food.setVisibility(View.VISIBLE);
        toy.setVisibility(View.VISIBLE);
        chat.setVisibility(View.VISIBLE);
        munny.setVisibility(View.VISIBLE);
        //  longEat.setVisibility(View.VISIBLE);
        // longHappy.setVisibility(View.VISIBLE);
        happy.setVisibility(View.VISIBLE);
        eat.setVisibility(View.VISIBLE);
        age.setVisibility(View.VISIBLE);
        level.setVisibility(View.VISIBLE);

        //By default - GONE
        sushi.setVisibility(View.GONE);
        fish.setVisibility(View.GONE);
        //  mediumEat.setVisibility(View.GONE);
        // smallEat.setVisibility(View.GONE);
        //  mediumHappy.setVisibility(View.GONE);
        //   smallHappy.setVisibility(View.GONE);
        rectangle.setVisibility(View.GONE);
        heartheart.setVisibility(View.GONE);
        notepad.setVisibility(View.GONE);
        dead.setVisibility(View.GONE);
        pricesushi.setVisibility(View.GONE);
        pricefish.setVisibility(View.GONE);
        hunger();
        happy();
    }

    public void hunger() {
        if (myCat.getHunger() > 80) {
            longEat.setVisibility(View.VISIBLE);
            mediumEat.setVisibility(View.GONE);
            smallEat.setVisibility(View.GONE);
        } else if ((myCat.getHunger() > 40) && (myCat.getHunger() <= 80)) {
            longEat.setVisibility(View.GONE);
            mediumEat.setVisibility(View.VISIBLE);
            smallEat.setVisibility(View.GONE);
        } else if ((myCat.getHunger() >= 0) && (myCat.getHunger() <= 40)) {
            longEat.setVisibility(View.GONE);
            mediumEat.setVisibility(View.GONE);
            smallEat.setVisibility(View.VISIBLE);
        }
    }
    public void happy() {
        if(myCat.getHappiness()> 80) {
            longHappy.setVisibility(View.VISIBLE);
            mediumHappy.setVisibility(View.GONE);
            smallHappy.setVisibility(View.GONE);
        } else if ((myCat.getHappiness()>40)&&(myCat.getHappiness()<= 80)) {
            longHappy.setVisibility(View.GONE);
            mediumHappy.setVisibility(View.VISIBLE);
            smallHappy.setVisibility(View.GONE);
        } else if ((myCat.getHappiness() >= 0)&&(myCat.getHappiness()<=40)) {
            longHappy.setVisibility(View.GONE);
            mediumHappy.setVisibility(View.GONE);
            smallHappy.setVisibility(View.VISIBLE);
        }
    }

    private void updateText(){
        levelText.setText(String.valueOf(myCat.getLevel()));
        munnyText.setText(String.valueOf(myCat.getMunny()));
        ageText.setText(String.valueOf(myCat.getAge()));

    }
    private void deadpet(){
        if(myCat.updateDeath()==true){
            dead.setVisibility(View.VISIBLE);
            dead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,addYourCat.class);
                    startActivity(intent);
                }

            });

            save();
            myCat.updateAwayTime((int)(currentTimeMillis()));
        }
    }

/*Credit Link:
     Icons made by:
     <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
     Background Photos From:
     <a href="https://pngtree.com/free-backgrounds">pngtree.com</a>
     Fonts made by:
     http://bythebutterfly.com
     Cat audios made by:
     http://www.orangefreesounds.com/kitty-meows/
     https://www.zapsplat.com/sound-effect-category/cats/
     */








}