package com.example.catpet;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundPlayer {

    private static SoundPool soundPool;
    private static int soundeat;
    private static int soundclick;
    private static int catlvl11;
    private static int catlvl12;
    private static int catlvl21;
    private static int catlvl22;
    private static int catlvl31;
    private static int catlvl32;
    private static int catlvl41;
    private static int catlvl42;
    private static int catlvl51;
    private static int catlvl52;
    private static int cattouch;






    public SoundPlayer(Context context){

        soundPool=new SoundPool(2, AudioManager.STREAM_MUSIC,0);
        soundeat=soundPool.load(context,R.raw.eatsound,1);
        soundclick=soundPool.load(context,R.raw.clicksound,1);
        catlvl11=soundPool.load(context,R.raw.catlvl11,1);
        catlvl12=soundPool.load(context,R.raw.catlvl12,1);
        catlvl21=soundPool.load(context,R.raw.catlvl21,1);
        catlvl22=soundPool.load(context,R.raw.catlvl22,1);
        catlvl31=soundPool.load(context,R.raw.catlvl31,1);
        catlvl32=soundPool.load(context,R.raw.catlvl32,1);
        catlvl41=soundPool.load(context,R.raw.catlvl41,1);
        catlvl42=soundPool.load(context,R.raw.catlvl42,1);
        catlvl51=soundPool.load(context,R.raw.catlvl51,1);
        catlvl52=soundPool.load(context,R.raw.catlvl52,1);
        cattouch=soundPool.load(context,R.raw.touchcat,1);

    }

    public void playeatsound(){
        soundPool.play(soundeat,1.0f,1.0f,1,0,1.0f);
    }
    public void playclicksound(){

        soundPool.play(soundclick,0.7f,0.7f,1,0,1.0f);
    }
    public void playcatlvl11(){
        soundPool.play(catlvl11,1.0f,1.0f,1,0,1.0f);
    }
    public void playcatlvl12(){

        soundPool.play(catlvl12,0.7f,0.7f,1,0,1.0f);
    }
    public void playcatlvl21(){
        soundPool.play(catlvl21,1.0f,1.0f,1,0,1.0f);
    }
    public void playcatlvl22(){

        soundPool.play(catlvl22,0.7f,0.7f,1,0,1.0f);
    }
    public void playcatlvl31(){
        soundPool.play(catlvl31,1.0f,1.0f,1,0,1.0f);
    }
    public void playcatlvl32(){

        soundPool.play(catlvl32,0.7f,0.7f,1,0,1.0f);
    }
    public void playcatlvl41(){
        soundPool.play(catlvl41,1.0f,1.0f,1,0,1.0f);
    }
    public void playcatlvl42(){

        soundPool.play(catlvl42,0.7f,0.7f,1,0,1.0f);
    }
    public void playcatlvl51(){
        soundPool.play(catlvl51,1.0f,1.0f,1,0,1.0f);
    }
    public void playcatlvl52(){

        soundPool.play(catlvl52,0.7f,0.7f,1,0,1.0f);
    }
    public void playtouchcat(){
        soundPool.play(cattouch,0.7f,0.7f,1,0,1.0f);

    }


}
