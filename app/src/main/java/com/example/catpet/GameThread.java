package com.example.catpet;

import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;

import static android.content.ContentValues.TAG;

public class GameThread extends Thread {

    private static final String TAG=GameThread.class.getSimpleName();
    GameView gameview;
    SurfaceHolder surfaceHolder; //surfaceholder object
    boolean isRunning; //to detect whether thread is running
    boolean running;
    long startTime, loopTime; //Loop start time
    long DELAY = 33; //delay in milis between screen refreshes
    private final static int MAX_FPS=50;
    private final static int MAX_FRAME_SKIPS=5;
    private final static int FRAME_PERIOD=1000/MAX_FPS;

    public void setRunning(boolean isRunning){
        this.isRunning=running;
    }



    public GameThread(SurfaceHolder surfaceHolder) {
        super();
        this.surfaceHolder = surfaceHolder;
        isRunning = true;
    }

    @Override
    public void run(){
        //Looping until the boolean is false

        long tickcount=0L;
        long begintime;
        long timediff=0;
        int sleeptime;
        int frameskipped;
        sleeptime=0;
        Log.d(TAG,"Starting Game Loop");
      Canvas canvas;
        while(isRunning){
            tickcount++;
            //locking the canvas
            canvas = surfaceHolder.lockCanvas(null);
            try{
            synchronized (surfaceHolder) {
                startTime = SystemClock.uptimeMillis();
                frameskipped = 0;//reset frame skip
                //updategame state
               // this.gameview.update();
              //  this.gameview.render(canvas);

                //calculate sleep time
                sleeptime = (int) (FRAME_PERIOD - timediff);
                //loop time
                loopTime = SystemClock.uptimeMillis() - startTime;
                //Pausing here to make sure we update the right amount per second
                if (loopTime < DELAY) {
                    try {
                        Thread.sleep(DELAY - loopTime);
                    } catch (InterruptedException e) {
                        Log.e("Interrupted", "Interrupted while sleeping");
                    }
                }
                while (loopTime < 0 && frameskipped < MAX_FRAME_SKIPS) {
                 //   this.gameview.update();
                    loopTime += FRAME_PERIOD;
                    frameskipped++;
                }
            }
            } finally{
                    if (canvas != null) {
                        synchronized (surfaceHolder) {
                            //unlocking the game
                            surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                                        }

                     }
        }
    }

    //Return whether thread is running
    public boolean isRunning(){
        return isRunning;
    }

    //sets the thread state, false = stopped, true = running
    public void setIsRunning(boolean state){
        isRunning = state;
    }
}
