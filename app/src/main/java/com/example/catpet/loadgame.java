package com.example.catpet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.System.currentTimeMillis;

public class loadgame extends AppCompatActivity {

    ImageButton catPawLogin;
    ImageButton logout;
    View view;
    EditText catName;
    private String catname;
    String line;

    Pet myPet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loadgame);
        catPawLogin=findViewById(R.id.catpawlogin);
        catName=findViewById(R.id.catname);
        logout=findViewById(R.id.logout);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
      catPawLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    // catname = catName.getText().toString();

                    String[] words = new String[8];
                    FileInputStream petFile;

                        petFile = openFileInput(catName.getText().toString() + ".pet");
                    BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(petFile)));


                    while ((line = br.readLine()) != null) {
                        words = line.split(" ");
                    }
                    br.close();


                    myPet = new Pet(words[0], Integer.parseInt(words[1]), Integer.parseInt(words[2]), Integer.parseInt(words[3]),
                            Integer.parseInt(words[4]), Integer.parseInt(words[5]), Integer.parseInt(words[6]),
                            Integer.parseInt(words[7]));
                    myPet.updateAwayTime((int) (currentTimeMillis() / 1000));
                    myPet.updateAge((int) (currentTimeMillis() / 1000));

                    Toast.makeText(loadgame.this, "Load Game from " + getFilesDir() + "/" + myPet.getName() + ".pet", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(loadgame.this, MainActivity.class);


                    //Go to main activity
                    startActivity(intent);
                    loadgame.this.finish();
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }



        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(loadgame.this, login.class);
                startActivity(intent);

            }
        });
    }
    public void save(){
        //save variable in local file by converting variable to string
        //name,age,level,munny,agetime,awaytime,hunger,happiness
        String petInfo = myPet.getName() + " " + String.valueOf(myPet.getAge()) + " " + String.valueOf(myPet.getLevel()) + " " +
                String.valueOf(myPet.getMunny()) + " " +String.valueOf(myPet.getHunger())+" "+String.valueOf(myPet.getHappiness())+" "+String.valueOf(myPet.getAgeTime())+" "+String.valueOf(myPet.getAwayTime());

        FileOutputStream outputStream;
        try{
            outputStream = openFileOutput(myPet.getName() + ".pet", Context.MODE_PRIVATE);
            outputStream.write(petInfo.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
