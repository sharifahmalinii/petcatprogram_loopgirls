package com.example.catpet;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public class Pet {
    private static String name;
    private static int age;
    private static int level;
    private static int munny;
    private static int ageTime;
    private static int awayTime;
    private static int hunger;
    private static int happiness;
    private static int counterhappiness = 0;
    private static int counterhunger = 0;

    // protected void onCreate(Bundle savedInstanceState) {
    //super.onCreate(savedInstanceState);
    //  setContentView(R.layout.activity_main);
    //}
    public Pet(){

    }
    //load progress
    public Pet(String name, int age, int level, int munny,int hunger, int happiness, int ageTime, int awayTime) {
        this.name = name;
        this.age = age;
        this.level = level;
        this.munny = munny;
        this.ageTime = ageTime;
        this.hunger = hunger;
        this.happiness = happiness;
        this.awayTime = awayTime;

    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public int getLevel() {
        return this.level;
    }

    public int getMunny() {
        return this.munny;
    }

    public int getAgeTime() {
        return this.ageTime;
    }

    public int getAwayTime() {
        return this.awayTime;
    }


    public int getHunger() {
        return this.hunger;
    }

    public int getHappiness() {
        return this.happiness;
    }

    public static void setAge(int age) {
        Pet.age = age;
    }
    public static void setLevel(int level) {
        Pet.level = level;
    }
    public static void setMunny(int munny) {
        Pet.munny = munny;
    }
    public static void setHunger(int hunger) {
        Pet.hunger = hunger;
    }

    public static void setHappiness(int happiness) {
        Pet.happiness = happiness;
    }

    public static void setName(String name) {
        Pet.name = name;
    }

    public static void setAgeTime(int ageTime) {
        Pet.ageTime = ageTime;
    }

    public static void setAwayTime(int awayTime) {
        Pet.awayTime = awayTime;
    }

    public void updateAwayTime(int time) {
        // 15 minutes
        if ((time - this.awayTime) / 60 > 0) {
            int hoursAway = (time - awayTime) / 60;
            updateHunger(-hoursAway);
            updateHappy(-hoursAway);
        }
        this.awayTime = time;
    }

    //Hunger
    public void updateHunger(int food) {

        if (hunger+food >=100)
            this.hunger = 100;
        else
            this.hunger = hunger + food;

        if (this.hunger > 80)
          this.counterhunger++;

        updateDeath();
        updateLevel();
    }

    //Happiness (if eat, play)
    public int updateHappy(int happy) {
        if (happiness+happy >= 100)
            // System.out.println("Your cat is on cloud nine!");
            this.happiness = 100;
        else
            this.happiness= happiness + happy;

        if (happiness > 80)
            this.counterhappiness++;

        updateDeath();
        updateLevel();
        return happiness;
    }

    //Pet is dead (hunger, play, time)
    public boolean updateDeath() {
        if(happiness<0 || hunger<0)
            return true;
        else
            return false;
    }

    //Pet age
    public int updateAge(int time) {
        //every 10 minutes decrease by 1
        if (time - ageTime >= 600) {
            this.age+=1;
            this.ageTime = time;
            return this.age;
        } else
            return this.age;
    }


    public int updateMunny(int munny){
        return this.munny+=munny;
    }


    public int updateLevel() {
        if (age == 2 && counterhappiness >= 3 && counterhunger >= 3)
            this.level = 2;
        else if (age == 3 && counterhappiness >= 6 && counterhunger >= 6)
            this.level = 3;
        else if (age == 4 && counterhappiness >= 9 && counterhunger >= 9)
            this.level = 4;
        else if (age >= 5 && counterhappiness >= 12 && counterhunger >= 12)
            this.level = 5;

        return this.level;

    }

}