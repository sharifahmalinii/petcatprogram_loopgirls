package com.example.catpet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class mazeclass extends AppCompatActivity {

    MediaPlayer music;
   // private TextView highscore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Gets rid of top border (complete fullscreen)
        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        //highscore.setText(String.valueOf(score.getScore()));
        BitmapFactory.Options options = new BitmapFactory.Options();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        options.outHeight = size.x;
        options.outWidth = size.y;

        music = MediaPlayer.create(this, R.raw.mazesong);
        music.setVolume(0.3F, 0.3F);
        music.setLooping(true);
        music.start();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mazeclass);
        // highscore.setText(String.valueOf(score.getScore()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        music.release();

    }


}
