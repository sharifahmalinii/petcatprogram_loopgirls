package com.example.catpet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class register extends AppCompatActivity {
    EditText email, password;
    Button signupbutton;
    TextView signin;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        auth = FirebaseAuth.getInstance();
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        signupbutton = findViewById(R.id.signup);
        signin = findViewById(R.id.signin);
        signupbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailuser = email.getText().toString();
                String passworduser= password.getText().toString();
                if(emailuser.isEmpty()){
                    email.setError("Please enter email id");
                    email.requestFocus();
                }
                else  if(passworduser.isEmpty()){
                    password.setError("Please enter your password");
                    password.requestFocus();
                }
                else  if(emailuser.isEmpty() && passworduser.isEmpty()){
                    Toast.makeText(register.this,"Fields Are Empty!",Toast.LENGTH_SHORT).show();
                }
                else  if(!(emailuser.isEmpty() && passworduser.isEmpty())){
                    auth.createUserWithEmailAndPassword(emailuser,passworduser).addOnCompleteListener(register.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                Toast.makeText(register.this,"Try Again",Toast.LENGTH_SHORT).show();
                            }
                            else {
                                startActivity(new Intent(register.this,addYourCat.class));
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(register.this,"Error Occurred!",Toast.LENGTH_SHORT).show();

                }
            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(register.this,login.class);
                startActivity(i);
            }
        });
    }
}