package com.example.catpet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.FileOutputStream;

import static java.lang.System.currentTimeMillis;

public class addYourCat extends AppCompatActivity {
    //EditText
    private EditText catName;

    private TextView youAdopted;

    private ImageButton catPawLogin;

    private ImageView petBox;

    private View view;

    private SoundPlayer sound;

    private String catname;
    MediaPlayer music;

    //firebase stuff
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDatabasereference;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private String UserID;

    Pet myPet;



    public static final String EXTRA_TEXT = "com.example.catpet.addyourcat.EXTRA_TEXT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_add_your_cat);

        music=MediaPlayer.create(this,R.raw.background);
        music.setVolume(0.3F,0.3F);
        music.setLooping(true);
        music.start();

        mAuth=FirebaseAuth.getInstance();
        mDatabase=FirebaseDatabase.getInstance();
      //  mDatabasereference=mDatabase.getReference();
       // FirebaseUser user =mAuth.getCurrentUser();
       // UserID=user.getUid();

        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );


        final SoundPlayer sound=new SoundPlayer(this);

        //EditText
        catName = (EditText)findViewById(R.id.catname);

        //TextViews
        youAdopted = findViewById(R.id.youadopted);

        catPawLogin = findViewById(R.id.catpawlogin);

        petBox = findViewById(R.id.petbox);


        //When we click the paw button
        catPawLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v){
                sound.playclicksound();
                view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
               catname = catName.getText().toString();
                newCat(catname);
                myPet = new Pet(catname,0,1,100,100,100,(int)(currentTimeMillis()/1000),(int)(currentTimeMillis()/1000));
                String petInfo =catname+ " " + String.valueOf(myPet.getAge()) + " " + String.valueOf(myPet.getLevel()) + " " +
                        String.valueOf(myPet.getMunny()) + " " +String.valueOf(myPet.getHunger())+" "+
                        String.valueOf(myPet.getHappiness())+" "+String.valueOf((int)currentTimeMillis())+" "+String.valueOf((int)currentTimeMillis());
                //to save the progress

                FileOutputStream outputStream;
                 try{
                outputStream = openFileOutput(myPet.getName() + ".pet", Context.MODE_PRIVATE);
                 outputStream.write(petInfo.getBytes());
                 Toast.makeText(addYourCat.this,"Saved To"+getFilesDir()+"/"+ myPet.getName()+".pet",Toast.LENGTH_LONG).show();
                  outputStream.close();
                } catch (Exception e) {
                e.printStackTrace();
                 }


               view = getCurrentFocus();
            }

        });
        //when create pet we must assigned new value initialize

        //create info to string


     // mDatabasereference.child("users").child(UserID).setValue(myPet);







    }
    @Override
    protected void onStart(){
        super.onStart();
        music.release();
    //    mAuth.addAuthStateListener(mAuthListener);

    }

    @Override
    protected void onPause(){
        super.onPause();
        music.release();

    }

    private void newCat(String catName){
        Intent intent = new Intent(addYourCat.this, MainActivity.class);
        intent.putExtra(EXTRA_TEXT, catname);

        //Go to main activity
        startActivity(intent);
        addYourCat.this.finish();
    }
}
