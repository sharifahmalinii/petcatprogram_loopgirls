package com.example.catpet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class Chat extends AppCompatActivity {
    //TextView
    private TextView unlockmore;
    //ImageView
    private ImageButton oyen1;
    private ImageView catlvl11;
    private ImageView catlvl12;
    private ImageView catlvl21;
    private ImageView catlvl22;
    private ImageView catlvl31;
    private ImageView catlvl32;
    private ImageView catlvl41;
    private ImageView catlvl42;
    private ImageView catlvl51;
    private ImageView catlvl52;

    //ImageButton
    private ImageButton user1;
    private ImageButton user2;
    private ImageButton user3;
    private ImageButton user4;
    private ImageButton user5;
    //sound effect
    private SoundPlayer sound;
    MediaPlayer music;

    private int catlevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Pet myCat = new Pet();
       final SoundPlayer sound=new SoundPlayer(this);

        music= MediaPlayer.create(this,R.raw.mazesong);
        music.setVolume(0.3F,0.3F);
        music.setLooping(true);
        music.start();


        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_chat);
        Random rand = new Random();
        final int gene = rand.nextInt(2);
        catlevel = myCat.getLevel();

        //TextView
        unlockmore = findViewById(R.id.unlockmore);

        //ImageView
        oyen1 = findViewById(R.id.oyen1);
        catlvl11 = findViewById(R.id.catlvl11);
        catlvl12 = findViewById(R.id.catlvl12);
        catlvl21 = findViewById(R.id.catlvl21);
        catlvl22 = findViewById(R.id.catlvl22);
        catlvl31 = findViewById(R.id.catlvl31);
        catlvl32 = findViewById(R.id.catlvl32);
        catlvl41 = findViewById(R.id.catlvl41);
        catlvl42 = findViewById(R.id.catlvl42);
        catlvl51 = findViewById(R.id.catlvl51);
        catlvl52 = findViewById(R.id.catlvl52);

        //ImageButton
        user1 = findViewById(R.id.user1);
        user2 = findViewById(R.id.user2);
        user3 = findViewById(R.id.user3);
        user4 = findViewById(R.id.user4);
        user5 = findViewById(R.id.user5);

        initialVisibility();


        oyen1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                initialVisibility();
                visible();
            }
        });

        user1.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                sound.playclicksound();
                initialVisibility();
                if(gene==0) {
                    catlvl11.setVisibility(View.VISIBLE);
                    sound.playcatlvl11();
                }else {
                    sound.playcatlvl12();
                    catlvl12.setVisibility(View.VISIBLE);
                }
            }
        });

        user2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                sound.playclicksound();
                initialVisibility();
                if(gene==0) {
                    catlvl21.setVisibility(View.VISIBLE);
                    sound.playcatlvl21();
                }else {
                    catlvl22.setVisibility(View.VISIBLE);
                    sound.playcatlvl22();
                }
            }
        });

        user3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                sound.playclicksound();
                initialVisibility();
                if(gene==0) {
                    catlvl31.setVisibility(View.VISIBLE);
                    sound.playcatlvl31();
                }else {
                    catlvl32.setVisibility(View.VISIBLE);
                    sound.playcatlvl32();
                }

            }
        });

        user4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                sound.playclicksound();
                initialVisibility();
                if(gene==0) {
                    catlvl41.setVisibility(View.VISIBLE);
                    sound.playcatlvl41();
                }else {
                    catlvl42.setVisibility(View.VISIBLE);
                    sound.playcatlvl42();
                }
            }
        });

        user5.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                sound.playclicksound();
                initialVisibility();
                if(gene==0) {
                    catlvl51.setVisibility(View.VISIBLE);
                    sound.playcatlvl51();
                }else {
                    catlvl52.setVisibility(View.VISIBLE);
                    sound.playcatlvl52();
                }

            }
        });

    }

    @Override
    protected void onPause(){
        super.onPause();
        music.release();

    }

    public void visible(){
        switch(catlevel){
            case 1:
            user1.setVisibility(View.VISIBLE);
            user2.setVisibility(View.GONE);
            user3.setVisibility(View.GONE);
            user4.setVisibility(View.GONE);
            user5.setVisibility(View.GONE);
            break;

            case 2:
                user1.setVisibility(View.VISIBLE);
                user2.setVisibility(View.VISIBLE);
                user3.setVisibility(View.GONE);
                user4.setVisibility(View.GONE);
                user5.setVisibility(View.GONE);
                break;

            case 3:
                user1.setVisibility(View.VISIBLE);
                user2.setVisibility(View.VISIBLE);
                user3.setVisibility(View.VISIBLE);
                user4.setVisibility(View.GONE);
                user5.setVisibility(View.GONE);
                break;

            case 4:
                user1.setVisibility(View.VISIBLE);
                user2.setVisibility(View.VISIBLE);
                user3.setVisibility(View.VISIBLE);
                user4.setVisibility(View.VISIBLE);
                user5.setVisibility(View.GONE);
                break;
            case 5:
                user1.setVisibility(View.VISIBLE);
                user2.setVisibility(View.VISIBLE);
                user3.setVisibility(View.VISIBLE);
                user4.setVisibility(View.VISIBLE);
                user5.setVisibility(View.VISIBLE);
                break;
            }


        }

    public void initialVisibility(){

        //By default - VISIBLE
        unlockmore.setVisibility((View.VISIBLE));

        //By default - GONE
        catlvl11.setVisibility(View.GONE);
        catlvl12.setVisibility(View.GONE);
        catlvl21.setVisibility(View.GONE);
        catlvl22.setVisibility(View.GONE);
        catlvl31.setVisibility(View.GONE);
        catlvl32.setVisibility(View.GONE);
        catlvl41.setVisibility(View.GONE);
        catlvl42.setVisibility(View.GONE);
        catlvl51.setVisibility(View.GONE);
        catlvl52.setVisibility(View.GONE);
        user1.setVisibility(View.GONE);
        user2.setVisibility(View.GONE);
        user3.setVisibility(View.GONE);
        user4.setVisibility(View.GONE);
        user5.setVisibility(View.GONE);

    }
}
