package com.example.catpet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;


    public class maze extends View {

        public maze(Context context) {
            super(context);
        }

        private enum Direction {
            UP, DOWN, LEFT, RIGHT
        }

        private Cell[][] cell;
        private Cell player, exit, munny1, munny2;
        private static final int COLUMN = 20, ROWS = 10; //column and row
        private static final float WALL_THICKNESS = 5;
        private float cellSize, hMargin, vMargin;
        private Paint wallPaint, playerPaint, exitPaint, munnyicon, munnyicon1;
        private Random random;

        public maze(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);

            wallPaint = new Paint();
            wallPaint.setColor(Color.BLACK);
            wallPaint.setStrokeWidth(WALL_THICKNESS); //maze's wall thickness


            playerPaint = new Paint();
            munnyicon = new Paint();
            munnyicon1 = new Paint();
            exitPaint = new Paint();
            random = new Random();
            createMaze();
        }

        private Cell getNeighbour(Cell cell) {
            ArrayList<Cell> neighbour = new ArrayList<>();

            //left neighbour
            if (cell.col > 0)
                if (!this.cell[cell.col - 1][cell.row].visited)
                    neighbour.add(this.cell[cell.col - 1][cell.row]);


            //right neighbour
            if (cell.col < COLUMN - 1)
                if (!this.cell[cell.col + 1][cell.row].visited)
                    neighbour.add(this.cell[cell.col + 1][cell.row]);


            //top neighbour
            if (cell.row > 0)
                if (!this.cell[cell.col][cell.row - 1].visited)
                    neighbour.add(this.cell[cell.col][cell.row - 1]);


            //bottom neighbour
            if (cell.row < ROWS - 1)
                if (!this.cell[cell.col][cell.row + 1].visited)
                    neighbour.add(this.cell[cell.col][cell.row + 1]);


            if (neighbour.size() > 0) {
                int index = random.nextInt(neighbour.size());
                return neighbour.get(index);
            }
            return null;
        }

        private void removeWall(Cell current, Cell next) {

            if (current.col == next.col && current.row == next.row + 1) {
                current.topWall = false;
                next.bottomWall = false;
            }

            if (current.col == next.col && current.row == next.row - 1) {
                current.bottomWall = false;
                next.topWall = false;
            }

            if (current.col == next.col + 1 && current.row == next.row) {
                current.leftWall = false;
                next.rightWall = false;
            }

            if (current.col == next.col - 1 && current.row == next.row) {
                current.rightWall = false;
                next.leftWall = false;
            }
        }

        private void createMaze() {
            Stack<Cell> stack = new Stack<>();
            Cell current, next;

            cell = new Cell[COLUMN][ROWS];

            for (int x = 0; x < COLUMN; x++) {
                for (int y = 0; y < ROWS; y++) {
                    cell[x][y] = new Cell(x, y); //x is column, y is row
                }
            }

            player = cell[0][0]; //top left of the cell/maze
            exit = cell[COLUMN - 1][ROWS - 1];
            current = cell[0][0];
            current.visited = true;

            do {
                next = getNeighbour(current);
                if (next != null) {
                    removeWall(current, next);
                    stack.push(current);
                    current = next;
                    current.visited = true;
                } else
                    current = stack.pop();
            } while (!stack.empty());
        }

        private class Cell {
            boolean
                    topWall = true,
                    leftWall = true,
                    bottomWall = true,
                    rightWall = true,
                    visited = false;

            int col, row;

            public Cell(int col, int row) {
                this.col = col;
                this.row = row;
            }
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.player);
            Bitmap c = BitmapFactory.decodeResource(getResources(), R.drawable.cathome);
            Bitmap d = BitmapFactory.decodeResource(getResources(), R.drawable.backgroundmaze);
            Bitmap e = BitmapFactory.decodeResource(getResources(), R.drawable.coin);

            // canvas.drawColor(Color.WHITE); //background of the game
            canvas.drawBitmap(d, 0, 0, null);

            int width = getWidth();
            int height = getHeight();

            if (width / height < COLUMN / ROWS)
                cellSize = width / (COLUMN + 1);
            else
                cellSize = height / (ROWS + 1);

            hMargin = (width - COLUMN * cellSize) / 2;
            vMargin = (height - ROWS * cellSize) / 2;

            canvas.translate(hMargin, vMargin);

            for (int x = 0; x < COLUMN; x++) {
                for (int y = 0; y < ROWS; y++) {
                    if (cell[x][y].topWall)
                        canvas.drawLine(
                                x * cellSize,
                                y * cellSize,
                                (x + 1) * cellSize,
                                y * cellSize,
                                wallPaint);

                    if (cell[x][y].leftWall)
                        canvas.drawLine(
                                x * cellSize,
                                y * cellSize,
                                x * cellSize,
                                (y + 1) * cellSize,
                                wallPaint);

                    if (cell[x][y].bottomWall)
                        canvas.drawLine(
                                x * cellSize,
                                (y + 1) * cellSize,
                                (x + 1) * cellSize,
                                (y + 1) * cellSize,
                                wallPaint);

                    if (cell[x][y].rightWall)
                        canvas.drawLine(
                                (x + 1) * cellSize,
                                y * cellSize,
                                (x + 1) * cellSize,
                                (y + 1) * cellSize,
                                wallPaint);
                }
            }
            int margin = (int) cellSize / 10;

            munny1 = cell[COLUMN - 4][ROWS - 4]; //Have to make these numbers static
            munny2 = cell[2][2];

            Rect playerRect = new Rect(player.col * (int) cellSize + margin, player.row * (int) cellSize + margin, (player.col + 1) * (int) cellSize - margin, (player.row + 1) * (int) cellSize - margin);
            Rect exitRect = new Rect(exit.col * (int) cellSize + margin, exit.row * (int) cellSize + margin, (exit.col + 1) * (int) cellSize - margin, (exit.row + 1) * (int) cellSize - margin);
            Rect munnyRect = new Rect(munny1.col * (int) cellSize + margin, munny1.row * (int) cellSize + margin, (munny1.col + 1) * (int) cellSize - margin, (munny1.row + 1) * (int) cellSize - margin);
            Rect munny2Rect = new Rect(munny2.col * (int) cellSize + margin, munny2.row * (int) cellSize + margin, (munny2.col + 1) * (int) cellSize - margin, (munny2.row + 1) * (int) cellSize - margin);
            canvas.drawBitmap(b, null, playerRect, playerPaint);
            canvas.drawBitmap(c, null, exitRect, exitPaint);

            canvas.drawBitmap(e, null, munnyRect, munnyicon);
            canvas.drawBitmap(e, null, munny2Rect, munnyicon1);

            if (player == munny1) {
                munnyicon.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                munnyicon.setXfermode(null);
                munnyicon.setColor(Color.TRANSPARENT);
            }

            if (player == munny2) {
                munnyicon1.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                munnyicon1.setXfermode(null);
                munnyicon1.setColor(Color.TRANSPARENT);
            }
        }

        private void movePlayer(Direction direction) {
            switch (direction) {
                case UP:
                    if (!player.topWall)
                        player = cell[player.col][player.row - 1];
                    break;
                case DOWN:
                    if (!player.bottomWall)
                        player = cell[player.col][player.row + 1];
                    break;
                case LEFT:
                    if (!player.leftWall)
                        player = cell[player.col - 1][player.row];
                    break;
                case RIGHT:
                    if (!player.rightWall)
                        player = cell[player.col + 1][player.row];
                    break;
            }


            checkExit();
            invalidate();

        }

        protected void checkExit() {
            if(player==exit) {
                Intent intent = new Intent(getContext(),MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                return true;

            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                float x = event.getX();
                float y = event.getY();

                float playerCenterX = hMargin + (player.col + 0.5f) * cellSize;
                float playerCenterY = vMargin + (player.row + 0.5f) * cellSize;

                float dx = x - playerCenterX;
                float dy = y - playerCenterY; //difference between where the player start and where it ends

                float absDx = Math.abs(dx);
                float absDy = Math.abs(dy);

                if (absDy > cellSize || absDy > cellSize) {
                    if (absDx > absDy) {
                        //move in x-direction
                        if (dx > 0)
                            movePlayer(Direction.RIGHT);
                            //move to the right

                        else
                            movePlayer(Direction.LEFT);
                        //move to the left
                    } else {
                        //move in y-direction
                        if (dy > 0)
                            movePlayer(Direction.DOWN);
                            //move down
                        else
                            movePlayer(Direction.UP);
                        //move upwards
                        ;
                    }
                }
                return true;

            }
            return super.onTouchEvent(event); //whenever there is 'touching' involved in the maze
        }

    }




